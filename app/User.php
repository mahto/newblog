<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hotel(){

        return $this->hasMany('App\Hotel');
    }

    public function role(){

        return $this->belongsTo('App\Role');
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        if($this->id == 1){

            return true;

        }

        return false;
    }

    public function isUser()
    {
        if(!$this->id == 1){

            return true;

        }

        return false;
    }






}
