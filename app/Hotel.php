<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     *
     */

    protected $fillable = [

        'user_id',
        'name'
    ];
    public function user(){

        return $this->belongsTo('App\User');

    }

    public function comments(){

        return $this->hasMany('App\Comment');
    }

    public function admin(){

        return $this->belongsTo('App\Admin');

    }
}
