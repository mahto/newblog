<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;

class WelcomePageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome()
    {
        $hotels = Hotel::all();

        return view('welcome', compact('hotels'));
    }
}
