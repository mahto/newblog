<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('IsUser');
    }


    public function index()
    {
        $hotels = Hotel::all();

        return view('user.index', compact('hotels'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $data = [

            'hotel_id' => $request->hotel_id,
            'author' => $user->name,
            'email' => $user->email,
            'body' => $request->body

        ];

        Comment::create($data);

        $request->session()->flash('comment_message', 'Your Comment has been submitted');

        return redirect()->back();
    }

    public function hotels($id)
    {

        $hotel = Hotel::findOrFail($id);

        $comments = $hotel->comments()->paginate(5);

        return view('user_comments', compact('hotel', 'comments'));
    }


}
