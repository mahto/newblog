<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Hotel;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;


class ShowCommentsController extends Controller
{

    public function show($id)
    {
        $post = Hotel::findOrFail($id);

        $comments = $post->comments;

        return view('show_comments', compact('comments'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showcomments($id)
    {

        $hotel = Hotel::findOrFail($id);

        $comments = $hotel->comments()->paginate(5);

        return view('show_comments', compact('hotel',  'comments'));

    }
}
