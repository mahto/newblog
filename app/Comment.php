<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [

        'hotel_id',
        'author',
        'email',
        'body',
        'is_active'
    ];


    public function hotel(){

        return $this->belongsTo('App\Hotel');
    }


}
