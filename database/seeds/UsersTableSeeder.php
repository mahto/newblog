<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'name'=> "Jhon",
            'role_id'=> 2,
            'is_active'=>1,
            'email' => "Jhon.@gmail.com",
            'password'=> bcrypt('doe')

        ]);
    }
}
