@extends('layouts.blog-post')


@section('content')



    <!-- Blog Post -->

    <!-- Title -->


    <!-- Author -->
    <p class="lead">
        by <a href="#">{{$hotel->user->name}}</a>
    </p>






    @if(Session::has('comment_message'))

        {{session('comment_message')}}

    @endif

    <hr>

    <!-- Blog Comments -->

    @if(Auth::check())
        <!-- Comments Form -->
        <div class="well">
            <h4>Leave a Comment:</h4>

            {!! Form::open(['method' => 'POST', 'action'=>'AdminCommentsController@store']) !!}

            <input type="hidden" name="hotel_id" value="{{$hotel->id}}">
            <div class="form-group">

                {!! Form::label('body' , 'Body') !!}
                {!! Form::textarea('body',null , ['class'=>'form-control' ,'rows'=> 3]) !!}

            </div>


            <div class="form-group">

                {!! Form::submit('Submit Comment',['class'=>'btn btn-primary']) !!}

            </div>


            {!!  Form::close() !!}


        </div>
    @endif

    <hr>




    <!-- Comment -->

    @if(count($comments)> 0)

        @foreach($comments as $comment)

            <div class="media">

                <div class="media-body">
                    <h4 class="media-heading">{{$comment->author}}

                    </h4>
                    <p>{{$comment->body}}</p>

                </div>
            </div>



        @endforeach

    @endif

@stop

@section('scripts')



    <script>

        $(function(){

            $(".comment-reply-container .toggle-reply").click(function(){

                $(this).next().slideToggle("slow");


            });


        });

    </script>



@stop


@extends('layouts.blog-post')


@section('content')




    <p class="lead">
        by <a href="#">{{$hotel->user->id}}</a>
    </p>














    <!-- Post Content -->


    @if(Session::has('comment_message'))

        {{session('comment_message')}}

    @endif

    <hr>

    <!-- Blog Comments -->

    @if(Auth::user())
        <!-- Comments Form -->
        <div class="well">
            <h4>Leave a Comment:</h4>

            {!! Form::open(['method' => 'POST', 'action'=>'AdminCommentsController@store']) !!}

            <input type="hidden" name="hotel_id" value="{{$hotel->id}}">
            <div class="form-group">

                {!! Form::label('body' , 'Body') !!}
                {!! Form::textarea('body',null , ['class'=>'form-control' ,'rows'=> 3]) !!}

            </div>


            <div class="form-group">

                {!! Form::submit('Submit Comment',['class'=>'btn btn-primary']) !!}

            </div>


            {!!  Form::close() !!}


        </div>
    @endif

    <hr>

    <!-- Posted Comments -->


    <!-- Comment -->

    @if(count($comments)> 0)

        @foreach($comments as $comment)

            <div class="media">
                {{--<a class="pull-left" href="#">--}}
                {{--<img height="64" class="media-object" src="{{$comment->photo}}" alt="">--}}
                {{--</a>--}}
                <div class="media-body">
                    <h4 class="media-heading">{{$comment->author}}
                        {{--<small>{{$comment->created_at->diffForHUmans()}}</small>--}}
                    </h4>
                    <p>{{$comment->body}}</p>


                    {{--@if(count($comment->replies)> 0)--}}

                    {{--@foreach($comment->replies as $reply)--}}

                    {{--@if($reply->is_active == 1)--}}
                    {{--<!-- Nested Comment -->--}}
                    {{--<div id = "nested-comment" class="media">--}}
                    {{--<a class="pull-left" href="#">--}}
                    {{--<img height = "64" class="media-object" src="{{$reply->photo}}" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="media-body">--}}
                    {{--<h4 class="media-heading">{{$reply->author}}--}}
                    {{--<small>{{$reply->created_at->diffForHUmans()}}</small>--}}
                    {{--</h4>--}}
                    {{--<p>{{$reply->body}}</p>--}}
                    {{--</div>--}}

                    {{--<div class="comment-reply-container">--}}

                    {{--<button class="toggle-reply btn btn-primary pull-right">Reply</button>--}}

                    {{--<div class="comment-reply col-sm-6">--}}

                    {{--{!! Form::open(['method' => 'POST', 'action'=>'CommentRepliesController@createReply']) !!}--}}
                    {{--<div class="form-group">--}}

                    {{--<input type="hidden" name="comment_id" value="{{$comment->id}}">--}}

                    {{--{!! Form::label('body' , 'Body') !!}--}}
                    {{--{!! Form::textarea('body',null , ['class'=>'form-control', 'rows' => 1]) !!}--}}

                    {{--</div>--}}


                    {{--<div class="form-group">--}}

                    {{--{!! Form::submit('submit',['class'=>'btn btn-primary']) !!}--}}

                    {{--</div>--}}



                    {{--{!!  Form::close() !!}--}}

                    {{--</div>--}}


                    {{--</div>--}}

                    {{--<!-- End Nested Comment -->--}}
                    {{--</div>--}}


                    {{--@endif--}}

                    {{--@endforeach--}}

                    {{--@endif--}}


                </div>
            </div>



        @endforeach

    @endif

@stop

@section('scripts')



    <script>

        $(function(){

            $(".comment-reply-container .toggle-reply").click(function(){

                $(this).next().slideToggle("slow");


            });


        });

    </script>



@stop
