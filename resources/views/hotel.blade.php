@extends('layouts.blog-post')


@section('content')



    <!-- Blog Post -->

    <!-- Title -->


    <!-- Author -->
    <p class="lead">
        by <a href="#">{{$hotel->user->name}}</a>
    </p>



    <!-- Post Content -->


    @if(Session::has('comment_message'))

        {{session('comment_message')}}

    @endif

    <hr>

    <!-- Blog Comments -->

    @if(Auth::check())
    <!-- Comments Form -->
    <div class="well">
        <h4>Leave a Comment:</h4>

        {!! Form::open(['method' => 'POST', 'action'=>'AdminCommentsController@store']) !!}

        <input type="hidden" name="hotel_id" value="{{$hotel->id}}">
        <div class="form-group">

            {!! Form::label('body' , 'Body') !!}
            {!! Form::textarea('body',null , ['class'=>'form-control' ,'rows'=> 3]) !!}

        </div>


        <div class="form-group">

            {!! Form::submit('Submit Comment',['class'=>'btn btn-primary']) !!}

        </div>


        {!!  Form::close() !!}


    </div>

    @endif


    <hr>

    <!-- Posted Comments -->


    <!-- Comment -->

    @if(count($comments)> 0)

        @foreach($comments as $comment)

            <div class="media">

                <div class="media-body">
                    <h4 class="media-heading">{{$comment->author}}
                    </h4>
                    <p>{{$comment->body}}</p>

                </div>


            </div>





        @endforeach

    @endif

    {{$comments->links()}}


@stop

@section('scripts')



    <script>

        $(function(){

            $(".comment-reply-container .toggle-reply").click(function(){

                $(this).next().slideToggle("slow");


            });


        });

    </script>


    {{--<div class = "row">--}}

        {{--<div class = "col-sm-6 col-sm-offset-5">--}}
            {{--{!! $comments->render() !!}--}}
        {{--</div>--}}

    {{--</div>--}}
@stop
