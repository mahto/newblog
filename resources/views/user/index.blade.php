@extends('layouts.admin')

@section('content')

    <h1>List of Available Hotels</h1>

    <table class="table">
        <thead>
        <tr>

            <th>Id</th>


        </tr>
        </thead>
        <tbody>

        @if($hotels)

            @foreach($hotels as $hotel)
                <tr>

                    <td>{{$hotel->name}}</td>
                    {{--//<td><a href="{{ route('admin.comments.show', $hotel->id) }}" >{{$hotel->name}}</a></td>--}}
                    <td><a href="{{ route('userhotel', $hotel->id) }}" >View Hotel</a></td>

                </tr>
            @endforeach

        @endif

        </tbody>
    </table>

    <div class = "row">

        {{--<div class = "col-sm-6 col-sm-offset-5">--}}
        {{--{{$posts->render()}}--}}
        {{--</div>--}}

    </div>

@stop