@extends('layouts.admin')

@section('content')



    <h1>Create Hotel</h1>

    <div class="col-sm-3">

        {!! Form::open(['method' => 'POST', 'action'=>'AdminHotelsController@store', 'files'=> true]) !!}
        <div class="form-group">

            {!! Form::label('name' , 'Name') !!}
            {!! Form::text('name' ,null, ['class'=>'form-control']) !!}

        </div>





        <div class="form-group">

            {!! Form::submit('Create Hotel',['class'=>'btn btn-primary']) !!}

        </div>

        {!!  Form::close() !!}



    </div>



    {{--<div class = "row">--}}

        {{--@include('includes.form_error')--}}

    {{--</div>--}}




@stop