@extends('layouts.admin')

@section('content')

    <h1>List of Available Hotels</h1>

    <table class="table">
        <thead>
        <tr>

            <th>Id</th>
            <th>Hotel</th>

        </tr>
        </thead>
        <tbody>

        @if($hotels)

            @foreach($hotels as $hotel)
                <tr>

                    <td>{{$hotel->id}}</td>
                    <td>{{$hotel->name}}</td>
                    <td><a href="{{ route('home.hotel', $hotel->id) }}" >View Comment</a></td>

                </tr>
            @endforeach

        @endif

        </tbody>
    </table>

    <div class = "row">

        <div class = "col-sm-6 col-sm-offset-5">
            {{$hotels->render()}}
        </div>

    </div>

@stop