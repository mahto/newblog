<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','WelcomePageController@welcome');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('admin','AdminHotelsController@index' );

Route::get('/hotel/{id}', ['as'=>'home.hotel', 'uses'=>'AdminHotelsController@hotel']);




//Routes For Admin Section

Route::group(['middleware' => 'admin'], function() {



    Route::resource('admin/users', 'AdminUsersController');


    Route::get('/admin/users', 'AdminUsersController@index')->name('admin.users.index');

    Route::get('/admin/users/create', 'AdminUsersController@create')->name('admin.users.create');

    Route::get('/admin/users/{id}/edit/', 'AdminUsersController@edit')->name('admin.users.edit');


    Route::resource('admin/hotels', 'AdminHotelsController');

    Route::get('/admin/hotels', 'AdminHotelsController@index')->name('admin.hotels.index');

    Route::get('/admin/hotels/create', 'AdminHotelsController@create')->name('admin.hotels.create');

    Route::get('/admin/hotels/{id}/edit/', 'AdminHotelsController@edit')->name('admin.hotels.edit');


    Route::resource('admin/comments', 'AdminCommentsController');

    Route::get('/admin/comments', 'AdminCommentsController@index')->name('admin.comments.index');

    Route::get('admin/comments/{id}' , 'AdminCommentsController@show')->name('admin.comments.show');



    Route::get('/admin/comments/create', 'AdminCommentsController@create')->name('admin.comments.create');



});

//Routes for User Section

    Route::get('/users', 'UsersController@index')->name('user.index');

    Route::post('/users/comments', 'AdminCommentsController@store')->name('user.store');

    Route::get('/hotels/{id}', ['as'=>'userhotel', 'uses'=>'usersController@hotels']);

    Route::get('comments/{id}', 'ShowCommentsController@show')->name('show_comments');

    Route::get('showcomments/{id}', ['as'=>'home.comments','uses'=>'ShowCommentsController@showcomments']);







// Logout Route

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
